import React,{useState,useEffect} from 'react'
import { View, Text,StyleSheet, ScrollView, FlatList,Image, TouchableHighlight, TouchableOpacity } from 'react-native'
import { Avatar, Card, IconButton,Divider } from 'react-native-paper';

Data1=[{
    id:"1",
    title:'Book',
    subtitle: "Consultation",
    image:require('./Image/Stethoscope.png'),
},
{
    id:"2",
    title:'Order',
    subtitle: "Medicine",
    image:require('./Image/Pill.png'),
},
{
    id:"3",
    title:'Lab',
    subtitle: "Test",
    image:require('./Image/litmus.png'),},
{
    id:"4",
    title:'Hello',
    subtitle: "Harshit",
    image:require('./Image/Stethoscope.png'),},
]

const FlatData = () => {
    const [data2,setData2] = useState([Data1])

    useEffect(() => {
        setData2(Data1)
    }, [])
    const renderItem = ({ item }) => (
        <>
        <View elevation={3} style={styles.container}>
        <View style={{width:24,height:24,paddingVertical:5}}>
        <Image style={{width:24,height:24,alignSelf:'center'}}  source={item.image} />
        </View>
       <View style={{minWidth:48,height:32,paddingHorizontal:4}}>
       <Text style={{fontSize:12,color:'rgba(0, 0, 0, 0.75)'}}>{item.title}</Text>
       <Text style={{fontSize:12,fontWeight:'bold',color:'rgba(0, 0, 0, 0.75)'}}>{item.subtitle}</Text> 
       </View>
       </View>

          
        </>
      );

    return (
         <FlatList
        data={data2}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      />
    )
}

export default FlatData


const styles=StyleSheet.create({
    container:{
        backgroundColor:'#FFFFFF',
        marginLeft:20,
        minWidth:96,
        flexDirection:'row',
        height:44,
        marginTop:20,
        shadowOpacity: 0.92,
        borderColor:'rgba(0, 0, 0, 0.12)',
        borderWidth:1,
        borderBottomColor:'#1E6785',
        borderBottomWidth:2,
        borderRadius:5,       
        paddingHorizontal:8,
        paddingVertical:6
    }
  })