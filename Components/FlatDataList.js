import React,{useState,useEffect} from 'react'
import { View, Text,StyleSheet, ScrollView, FlatList,Image } from 'react-native'
import { Button } from 'react-native-paper';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import { LinearGradient } from 'expo-linear-gradient';

Data=[{
    id:1,
    name:"Appointment",
    spec:'Cardio Specialist',
    name1:"Todays Appointment",
    name2:"04:00 pm - 05:00 pm",
    color1:['rgba(0, 65, 106, 1)','rgba(30, 103, 133, 1)'],
    color2:'#fff',
    color3:'#fff',
    image:require('./Image/image1.png'),
    wid:101,
    heig:88,
    top:33
},
{
    id:2,
    name:"Lab Test",
    spec:'Blood Test',
    name1:"Home Visit",
    name2:"08:00 pm - 08:00 pm",
    color1:['#EAF6FA', '#EAF6FA'],
    color2:'#1E6785',
    color3:'#000',
    image:require('./Image/image2.png'),
    image1:require('./Image/Drlogo.png'),
    wid:101,
    heig:88,
    top:33
},
{
    id:3,
    name:"Exercise",
    spec:'Morning',
    name1:"Todays Appointment",
    name2:"04:00 pm - 05:00 pm",
    color1:['rgba(0, 65, 106, 1)','rgba(30, 103, 133, 1)'],
    color2:'#fff',
    color3:'#fff',
    image:require('./Image/image3.png'),
    wid:86,
    heig:98,
    top:25
},
{
    id:4,
    name:"Diet ",
    spec:'BreakFast',
    name1:"Clear the wound by",
    name2:"dettol & put Betadine on it",
    color1:['#EAF6FA', '#EAF6FA'],
    color2:'#1E6785',
    color3:'#000',
    image:require('./Image/image4.png'),wid:101,
    heig:114,
    top:21
},
   
    {
        id:5,
       name:"Wound Care",
       spec:'Morning',
       name1:"1. Azax 500",
       name2:"2. Amlopress- AT ",
       color1:['rgba(0, 65, 106, 1)','rgba(30, 103, 133, 1)'],
       color2:'#fff',
       color3:'#fff',
       image:require('./Image/image5.png'),
       wid:101,
       heig:120,
       top:17
    },

       {
        id:6,
       name:"Madication",
       spec:'Morning',
       name1:"Todays Appointment",
       name2:"04:00 pm - 05:00 pm",
       color1:['#EAF6FA', '#EAF6FA'],
       color2:'#1E6785',
       color3:'#000',
       image:require('./Image/image6.png'),
       wid:117,
       heig:98,
       top:30
    },
]

const FlatDataList = () => {
    const [data1,setData1] = useState([])

    useEffect(() => {
        setData1(Data)
    }, [])
    const renderItem = ({ item }) => (
     
                <LinearGradient
                colors={item.color1}
                start={[0.0, 0.0]}
                end={[1.0, 1.0]}
                style={styles.container}>
   <View >
        <Text style={{ width:154,
        height:28,
       fontWeight:'600',
fontSize: 18,
color:item.color2}}>{item.name}</Text>
        <Text style={{width:111,
        height:20,
        marginTop:4,
        fontWeight:'600',
        fontSize: 14,
        color:item.color2}}>{item.spec}</Text>
        
               </View>
               <View style={{position:'absolute',left:180,top:5}}>
  <AnimatedCircularProgress
  size={60}
  width={12}
  fill={25}
  tintColor="#2F80ED"
  onAnimationComplete={() => console.log('onAnimationComplete')}
  backgroundColor="#E8F1FD">
    {
    (fill) => (
      <Text>
        { "25%" }
      </Text>
    )
  }
      </AnimatedCircularProgress>
  </View>
               <View style={{
    borderStyle: 'dashed',
    borderWidth: 1,
    borderRadius: 1,
    borderColor:item.color3,
    width:228,
    height:0,
    top:8
  }}>
</View>
<View>
<Text style={{ position:'absolute',
        width: 119,
        height: 18,
        top: 18,
        color:item.color2,
        fontWeight:'500',
        fontSize:12}}>{item.name1}</Text>
        <Text style={{ position:'absolute',
        width:121,
        height:16,
        top:35,
        fontWeight:'bold',
        fontSize: 12,
        color:item.color2,
         lineHeight: 16}}>{item.name2}</Text>
</View>
<Image source={item.image1} style={styles.logo1} />
<Button mode="contained"
style={styles.button1}
>
   Join Call
  </Button>
<View style={{width:101,
        height:98,
          top: item.top,
          left: 140,
        justifyContent: 'center',
    }}>
<Image source={item.image} style={{ width:item.wid,
        height:item.heig,
        alignSelf:'center',}} />
    </View>
  </LinearGradient>
      );

    return (
        <View>
         <FlatList
         style={{marginTop:20}}
        data={data1}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      />
    
      </View>
      
    )
}

export default FlatDataList


const styles=StyleSheet.create({
    container:{
       
        width:252,
        marginLeft:20,
        height:192,
         borderRadius:5,
        marginBottom:20,
        shadowRadius:20,
        paddingLeft:12,
        paddingTop:12
    },
    heading:{
        width:154,
        height:28,
       fontWeight:'600',
fontSize: 18,
color:'#FFFFFF'
    },
    subheading:{
        width:111,
        height:20,
        marginTop:4,
        fontWeight:'600',
        fontSize: 14,
        color:'#FFFFFF'
    },
    heading1:{
        position:'absolute',
        width: 119,
        height: 18,
        top: 18,
        color:'#FFFFFF',
        fontWeight:'500',
        fontSize:12
    },
    subheading1:{
        position:'absolute',
        width:121,
        height:16,
        top:35,
        fontWeight:'bold',
        fontSize: 12,
        color:'#FFFFFF',
         lineHeight: 16
        
    },
    button1:{
        position:'absolute',
        paddingHorizontal:0,
        paddingVertical:5,
        width: 128,
        height: 46,
        left: 12,
        top: 134,
        backgroundColor:"#ECB02D",
        
        
    },
    logo1:{
        position: "absolute",
width: 76,
height: 24,
left: 163,
top: 81,
    }
  })