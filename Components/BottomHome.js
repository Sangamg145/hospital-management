import React,{useState,useEffect} from 'react'
import { View, Text,Image,Animated,FlatList } from 'react-native'
import { Slider,Icon } from 'react-native-elements';
import { LinearGradient } from 'expo-linear-gradient';
import { Button } from 'react-native-paper';
Data=[{
    id:1,
    name:"Swasthya Seva",
    spec:'Quality Care on Call',
    name1:"Todays Appointment",
    name2:"04:00 pm - 05:00 pm",
    color1:['rgba(0, 65, 106, 1)', 'rgba(30, 103, 133, 0.5)'],
    color2:'#fff',
    color3:'#fff',
    image:require('./Image/image31.png'),
},
{
    id:2,
    name:"Lab Test",
    spec:'Blood Test',
    name1:"Home Visit",
    name2:"08:00 pm - 08:00 pm",
    color1:['#EAF6FA', '#EAF6FA'],
    color2:'#1E6785',
    color3:'#000',
    image:require('./Image/image2.png'),
    image1:require('./Image/Drlogo.png'),
},
{
    id:3,
    name:"Exercise",
    spec:'Morning',
    name1:"Todays Appointment",
    name2:"04:00 pm - 05:00 pm",
    color1:['rgba(0, 65, 106, 1)', 'rgba(30, 103, 133, 0.5)'],
    color2:'#fff',
    color3:'#fff',
    image:require('./Image/image3.png'),},
{
    id:4,
    name:"Diet ",
    spec:'BreakFast',
    name1:"Clear the wound by",
    name2:"dettol & put Betadine on it",
    color1:['#EAF6FA', '#EAF6FA'],
    color2:'#1E6785',
    color3:'#000',
    image:require('./Image/image4.png'),},
   
    {
        id:5,
       name:"Wound Care",
       spec:'Morning',
       name1:"1. Azax 500",
       name2:"2. Amlopress- AT ",
       color1:['rgba(0, 65, 106, 1)', 'rgba(30, 103, 133, 0.5)'],
       color2:'#fff',
       color3:'#fff',
       image:require('./Image/image5.png'),},

       {
        id:6,
       name:"Madication",
       spec:'Morning',
       name1:"Todays Appointment",
       name2:"04:00 pm - 05:00 pm",
       color1:['#EAF6FA', '#EAF6FA'],
       color2:'#1E6785',
       color3:'#000',
       image:require('./Image/image6.png'),},
]
export default function BottomHome() {
        const [data1,setData1] = useState([])
    
        useEffect(() => {
            setData1(Data)
        }, [])

        const renderItem = ({ item }) => (
            <LinearGradient
            colors={['rgba(49, 150, 199, 1)','rgba(71, 162, 206, 1)','rgba(132, 192, 220, 1)']}
            style={{width:264,height:248,backgroundColor:'#fff',borderRadius:5,padding:12,marginLeft:20,}}
            >
              <View >
        <Text style={{ width:239,
        height:28,
       fontWeight:'600',
fontSize: 24,
color:item.color2}}>{item.name}</Text>
        <Text style={{width:121,
        height:20,
        marginTop:4,
        fontWeight:'600',
        fontSize: 16,
        color:item.color2}}>{item.spec}</Text>
<View 
style={{ position:'absolute',
paddingHorizontal:12,
paddingVertical:8,
width: 121,
height: 36,
left: 12,
top: 68,
backgroundColor:"rgba(255, 255, 255, 1)",
borderRadius:5,
}}>
<Text style={{textAlign:'center',color:'rgba(30, 103, 133, 1)',fontWeight:'bold',fontSize:14}}>Start Call Now</Text>
  </View>
  <Image source={item.image} style={{width:163,
        height:122,
        left: 87.5,
        borderBottomRightRadius:5,
         top: 114,
         position:'absolute',
         justifyContent: 'center',
         alignItems: 'center',}} />
               </View>
                </LinearGradient>
 );
    return (
        <View>
        <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
            <View style={{width:164,height:232,backgroundColor:'#000000',padding:12,borderWidth:1,borderRadius:8,borderColor:'rgba(0, 0, 0, 0.12)'}}>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <Text style={{color:'#fff'}}>TBD</Text>
        <View style={{ width:20,height:20,backgroundColor:'#fff',borderRadius:20}}>
      </View>
</View>
      <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
      <Text style={{color:'#fff',fontSize:24}}>Reading</Text>
      <Text style={{color:'rgba(250, 253, 253, 0.7)',fontSize:13,marginTop:10}}>Units</Text>
      </View>
        <View style={{flexDirection:'row',width:100,marginTop:25}}>
        <Text style={{color:'#fff'}}>160</Text>
        <View style={{ borderStyle:'dotted', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:18}}>
        <Text style={{color:'#fff'}}>140</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:18}}>
        <Text style={{color:'#fff'}}>120</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:18}}>
        <Text style={{color:'#fff'}}>100</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(250, 253, 253, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
      <Image style={{width:107,height:78,
        alignSelf:'center',position:'absolute',left:35,top:-66}}  source={require('./Image/Vector1.png')} />
</View>
  
        </View>


        <View style={{width:164,height:232,backgroundColor:'#fff',borderWidth:1,borderRadius:8,padding:12,borderColor:'rgba(0, 0, 0, 0.12)'}}>
        
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <Text style={{color:'#000'}}>TBD</Text>
        <View style={{ width:20,height:20,backgroundColor:'#000',borderRadius:20}}>
      </View>
      </View>
      <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
      <Text style={{color:'#000',fontSize:24}}>Reading</Text>
      <Text style={{color:'rgba(0,0,0, 0.7)',fontSize:13,marginTop:10}}>Units</Text>
      </View>
        <View style={{flexDirection:'row',width:100,marginLeft:-8,marginTop:27}}>
        <Text>100</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(0,0,0, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:18}}>
        <Text>80</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(0,0,0, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:18}}>
        <Text>70</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(0,0,0, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
</View>
<View style={{flexDirection:'row',width:100,marginTop:18}}>
        <Text>60</Text>
        <View style={{ borderStyle:'dashed', borderWidth: 0.5, borderColor:'rgba(0,0,0, 0.7)', 
        width:"90%", marginHorizontal:20, height:0, top:10}}>
      </View>
      <Image style={{width:107,height:78,
        alignSelf:'center',position:'absolute',left:35,top:-66}}  source={require('./Image/Vector2.png')} />

</View>
        </View>
        </View>


        <View style={{flexDirection:'row',justifyContent:'space-evenly',marginTop:30}}>
            <View style={{width:164,height:96,padding:12,borderWidth:1,borderRadius:5,borderColor:'rgba(0, 0, 0, 0.12)'}}>
            <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <View>
        <Text style={{fontSize:20}}>Heart Rate</Text>
        </View>
        <View>
        <Image style={{width:28,height:28}}  source={require('./Image/heart.png')} />
      </View>
</View>
<View style={{flexDirection:'row',width:132,height:36,alignItems:'center',marginTop:8}}>
<Text style={{fontSize:24}}>71</Text>
<Text style={{marginTop:5}}> BPM</Text>
</View>
        </View>



        <View style={{width:164,height:96,backgroundColor:'#fff',borderWidth:1,borderRadius:5,padding:12,borderColor:'rgba(0, 0, 0, 0.12)'}}>
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <View style={{flexDirection:'row',width:140,height:28,alignItems:'center'}}>
        <Text style={{fontSize:20}}>TBD</Text>
        </View>
        
</View>
<View style={{flexDirection:'row',width:132,height:36,alignItems:'center',marginTop:8}}>
<Text style={{fontSize:24}}>Reading</Text>
<Text style={{marginTop:5}}> Units</Text>
</View>
        </View>
        </View>


        <View style={{alignSelf:'center',marginTop:30}}>
        <View style={{width:343,height:240,backgroundColor:'#fff',borderWidth:1,borderRadius:5,padding:12,borderColor:'rgba(0, 0, 0, 0.12)'}}>
        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
        <View>
        <Text style={{fontSize:22}}>How are you Feeling Today ?</Text>
        </View>    
        </View>
 <View style={{alignSelf:'center',marginTop:30}}>
 <Image style={{width:36,height:36,alignSelf:'center'}}  source={require('./Image/emoji1.png')} />
 <Text>Very Happy</Text>
 </View>
 <View style={{flexDirection:'row',marginTop:30}}>
 <Image style={{width:24,height:24,marginLeft:0}}  source={require('./Image/emoji2.png')} />
 <Image style={{width:24,height:24,marginLeft:50}}  source={require('./Image/emoji3.png')} />
 <Image style={{width:24,height:24,marginLeft:50}}  source={require('./Image/emoji5.png')} />
 <Image style={{width:24,height:24,marginLeft:50}}  source={require('./Image/emoji5.png')} />
 <Image style={{width:24,height:24,marginLeft:50}}  source={require('./Image/emoji6.png')} />
 </View>
 <View style={{marginTop:15}}>
 <Slider
    value={50}
    maximumValue={50}
    minimumValue={20}
    step={1}
    trackStyle={{ height: 10, backgroundColor: 'tomato' }}
    thumbStyle={{ height: 28, width: 28, backgroundColor: 'tomato' }}
    thumbProps={{
    }}/>
 </View>
        </View>
        </View>


        <View style={{marginTop:30,marginBottom:5}}>
        <FlatList
        style={{marginBottom:20}}
        data={data1}
        renderItem={renderItem}
        keyExtractor={item => item.id}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
      />        
        </View>
        </View>
    )
}
