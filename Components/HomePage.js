import * as React from 'react';
import {StyleSheet,View,Text,Image} from 'react-native';
import { Avatar, Card, IconButton,Divider } from 'react-native-paper';

const HomePage = () => (
  <View style={styles.container}>
    <View style={styles.container1}>
<View style={{width:55,height:55,borderWidth:1,left: 18.5,top: 26.5,borderRadius:30,borderColor:'#1E6785'}}>
<Image style={{width:52,height:52}} source={require('./Image/userlogo.png')} />
</View>
   <Text style={{width:148,height:36,left: 32.5,
   fontWeight:'bold',fontSize:22,fontFamily:'sans-serif',
   color:'#333333',
top: 24,}}>Hello Harshit</Text>

<Text style={{width:148,height:36,left: -110,color:'#4F4F4F',
top: 54,}}>Have a Great Day !</Text>
<Image style={{width:27,height:27,left:-40.5,
top: 26.5}} source={require('./Image/calendar.png')} />
<Image style={{width:26,height:26,left: -35.5,
top: 26.5}} source={require('./Image/bell.png')} />
    </View>
  </View>
);

export default HomePage;

const styles=StyleSheet.create({
  container:{
    width: "100%",
    height: 144,
    borderColor: '#000',
    borderWidth: 0.3,
    borderBottomEndRadius:10,
    borderBottomStartRadius:10,    
  },
  container1:{
    top: 44,
    flexDirection:'row',

    
  },
  icon:{
    flexDirection:'row',
    marginHorizontal:10
  },
  devider:{
    marginTop:20
  },
  name:{
width: 148,
height: 36,
left: 88,
top: 68,

fontWeight:'600',
fontSize: 24,

  }
  
})