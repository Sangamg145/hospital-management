import React from 'react'
import { View, Text,FlatList,Image } from 'react-native'
import { AnimatedCircularProgress } from 'react-native-circular-progress';

Data1 =[
    {
        id:1,
        name1:'5230',
        name2:'Steps',
        image:require('./Image/image11.png'),
        color:'#D5ECF6',
        color1:'#1E6785',
        wi:18,
        hi:24
    },
    {
        id:2,
        name1:'52',
        name2:'kcal',
        image:require('./Image/image12.png'),
        color:'#FBEDD0',
        color1:'#ECB02D',
        wi:18,
        hi:24
    },
    {
        id:3,
        name1:'6',
        name2:'Hours',
        image:require('./Image/Group.png'),
        color:'#E8F1FD',
        color1:'#2F80ED',
        wi:24,
        hi:18
    },
]

const renderItem = ({ item }) => (
    <View style={{flexDirection:'row',height:48,marginTop:20 }}>
  <View style={{width:48,height:48,backgroundColor:item.color,borderRadius:25,justifyContent:'center',
  alignItems:'center',paddingHorizontal:12,paddingVertical:15,}}>
  <Image style={{width:item.wi,height:item.hi}}  source={item.image} />
  </View>
  <Text style={{fontSize:16,fontWeight:'bold',color:item.color1,marginTop:12,marginLeft:5}}>{item.name1}</Text>
  <Text style={{fontSize:14,color:item.color1,marginTop:13,marginLeft:5}}>{item.name2}</Text>
  </View>
  );

const GraphPage = () => {
    
    return (
        <View style={{height:300}}>
            <View style={{flexDirection:'row',justifyContent:'space-between',marginHorizontal:17}}>
            <Text style={{fontSize:20}}>My Adherances</Text>
            <Text style={{fontSize:24,fontWeight:'bold'}}>75%</Text>
        </View>
    <View style={{flexDirection:"row"}}>
        <View>
    <View style={{top:10,left:16,width:200,height:200}}>
        <AnimatedCircularProgress
  size={200}
  width={15}
  fill={80}
  tintColor="#1E6785"
  backgroundColor="#D5ECF6"
   />
   </View>
  <View style={{position:'absolute',left:55,top:50}}>
  <AnimatedCircularProgress
  size={120}
  width={15}
  fill={70}
  tintColor="#2F80ED"
  onAnimationComplete={() => console.log('onAnimationComplete')}
  backgroundColor="#E8F1FD" />
  </View>
        
  <View style={{position:'absolute',left:35,top:30}}>
  <AnimatedCircularProgress
  size={160}
  width={15}
  fill={60}
  tintColor="#ECB02D"
  onAnimationComplete={() => console.log('onAnimationComplete')}
  backgroundColor="#FBEDD0" />
  </View>
  </View>
  <View style={{marginLeft:40}}>
  <View style={{width:132,height:205}}>
  <FlatList
        data={Data1}
        renderItem={renderItem}
        keyExtractor={item => item.id}
      />
  </View>
  </View>
  </View>
  <View style={{
    borderStyle: 'dotted',
    borderWidth: 1,
    borderRadius: 1,
    borderColor:'rgba(0, 0, 0, 0.25)',
    width:"90%",
    marginHorizontal:20,
    height:0,
    top:25
  }}>
</View>
        </View>
    )
}

export default GraphPage
