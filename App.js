import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View,SafeAreaView,ScrollView,Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import FlatData from './Components/FlatData';
import FlatDataList from './Components/FlatDataList';
import GraphPage from './Components/GraphPage';
import HomePage from './Components/HomePage';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import BottomHome from './Components/BottomHome';

function HomeScreen() {
  return (
    <ScrollView style={styles.container}>
       <HomePage />
      <FlatData />
      <FlatDataList />
      <GraphPage />
      <BottomHome />
    </ScrollView>
  );
}



function DailyLogs() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Settings!</Text>
    </View>
  );
}

function Rx() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Settings!</Text>
    </View>
  );
}

function DashBoard() {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Settings!</Text>
    </View>
  );
}
const Tab = createMaterialBottomTabNavigator();



export default function App() {
  return (
  
    <NavigationContainer >
      <Tab.Navigator
        initialRouteName="Home"
        activeColor="#1E6785"
        inactiveColor="#828282"
        barStyle={{ backgroundColor: '#fff' }}>
        <Tab.Screen name="Home" component={HomeScreen}
         options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24,backgroundColor:'#fff'}}  source={require('./Components/Image/Stethoscope.png')} />
          )
        }}
         />
        <Tab.Screen name="Rx" component={Rx} 
         options={{
          tabBarLabel: 'Rx',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24}}  source={require('./Components/Image/Medical_Annual.png')} />
          )
        }}
        />
        <Tab.Screen name="DailyLogs" component={DailyLogs}
         options={{
          tabBarLabel: 'DailyLogs',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24}}  source={require('./Components/Image/Frame.png')} />
          )
        }}
         />
          <Tab.Screen name="DashBoard" component={DashBoard}
         options={{
          tabBarLabel: 'DashBoard',
          tabBarIcon: ({ color }) => (
            <Image style={{width:24,height:24}}  source={require('./Components/Image/Union.png')} />
          )
        }}
         />
        

      </Tab.Navigator>
    </NavigationContainer>
     
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor:'#fff',
    shadowOpacity: 0.2,       
  
  },
});
